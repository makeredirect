package MakeRedirect;
use strict;
use warnings;
use MakeRedirect::URI;
use MakeRedirect::Rule;
use Text::CSV;
use Text::Locus;
use Carp;

our $VERSION = '0.10';

sub new {
    my ($class, $output, @args) = @_;
    my $self = bless {}, $class;
    my $modname = __PACKAGE__ . '::Output::' . $output;
    my $modpath = $modname;
    $modpath =~ s{::}{/}g;
    $modpath .= '.pm';
    $self->{output} = eval { require $modpath; $modname->new(@args) };

    if ($@) {
        if ($@ =~ /Can't locate $modpath/) {
	    croak "unknown output module: $output\n"
        }
        croak $@;
    }
    
    return $self;
}

sub scheme {
    my ($self, $scheme) = @_;
    if ($scheme) {
	$self->{scheme} = $scheme;
    }
    $self->{scheme};
}

sub getopt {
    my $self = shift;
    $self->{output}->getopt;
}

sub output {
    my $self = shift;
    $self->{output}->open;
    foreach my $host (sort keys %{$self->{rules}}) {
	$self->{output}->ruleset($host, $self->{rules}{$host});
    }
    $self->{output}->close;
}

sub error {
    my $self = shift;
    warn "@_\n";
}

sub read {
    my ($self, $file, %opts) = @_;

    my $skiplines = delete $opts{skip};
    my $filename = delete $opts{filename};
    my $fh;
    if (ref($file) eq 'GLOB') {
	open($fh, '<&', $file)
	    or croak "can't dup file handle: $!";
	$filename = '<filehandle>' unless $filename;
    } else {
	open($fh, '<:encoding(utf8)', $file)
	    or croak "Can't open $file: $!";
	$filename = $file unless $filename;
    }
    
    use constant {
	SRC => 0,
	DST => 1,
	FLG => 2,
	MF  => 2,
	NF  => 3
    };

    my $csv = Text::CSV->new( { binary => 1 } )
	or croak "Cannot use CSV: ".Text::CSV->error_diag();
    my $line = 0;
    my $def_flags = 0;
    while (my $row = $csv->getline($fh)) {
	$line++;
        next if $skiplines && $line <= $skiplines;
        my $nf = @$row;
        if ($nf == 0) {
	    next
        }
        if ($nf < MF) {
	    $self->error("$filename:$line: too few fields; ignoring line");
	    next;
        }
        if ($nf > NF) {
	    $self->error("$filename:$line: too many fields; ignoring surplus ones");
	    next;
        }

	my $flags = $def_flags;
	if ($nf == NF) {
	    my %t = (
		www => RF_WWW,
		exact => [ RF_EXACT, RF_PATH_MASK ],
		nosub => [ RF_NOSUB, RF_PATH_MASK ],
		noquery => RF_NOQUERY,
		temp => RF_TEMP
	    );

	    foreach my $x (split /\s+|[|,]/, $row->[FLG]) {
		my $neg;
		if ($x =~ s{^([-+])(.*)}{$2}) {
                    $neg = ($1 eq '-');
		}
		if (exists($t{$x})) {
		    my $v = $t{$x};
		    $v = [ $v, $v ] unless ref($v);
		    if ($neg) {
			$flags = ($flags & ~$v->[1]) | (($flags & $v->[1]) & ~$v->[0]);
		    } else {
			$flags = ($flags & ~$v->[1]) | $v->[0];
		    }
		} else {
		    $self->error("$filename:$line: warning: unknown flag $x");
		}
	    }
	}
	if ($row->[SRC] eq '-') {
	    $def_flags = $flags;
	    next;
	}
	
	my $src = new MakeRedirect::URI($row->[SRC],
					scheme => $self->{scheme});
	my $dst = new MakeRedirect::URI($row->[DST],
					scheme => $self->{scheme},
	                                path => '/');
        if ($src == $dst) {
	    $self->error("$filename:$line: source and destination are the same");
	    next;
        }

	if (!$src->path) {
	    $src->path('/');
	    $flags &= ~MakeRedirect::Rule::RF_EXACT;
	}

	if ($dst->query) {
	    $flags |= MakeRedirect::Rule::RF_NOQUERY;
	}
	
	my $host = $src->host // '';
	my $locus = new Text::Locus($filename, $line);
	if (my $orig = $self->{rules}{$host}{$src->canonical}) {
	    if ($orig->dst == $dst) {
		$self->error("$locus: duplicate rule (original declaration at "
		             . $orig->locus . "); ignoring");
	        next;
	    } else {
		$self->error("$locus: redefinition of the rule at ("
		             . $orig->locus . ")\n");
	    }
	}
	$self->{rules}{$host}{$src->canonical} =
	    new MakeRedirect::Rule($src, $dst, $flags, $locus);
    }
    $csv->eof or $self->error($csv->error_diag());
    close $fh;
}

1;    
