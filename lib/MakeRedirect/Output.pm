package MakeRedirect::Output;

=head1 NAME

MakeRedirect::Output - A base class for MakeRedirect output modules.

=cut

use strict;
use warnings;
use Carp;
use File::Spec;
use Getopt::Long qw(:config gnu_getopt no_ignore_case require_order);
use Pod::Man;
use Pod::Usage;
use Pod::Find qw(pod_where);

sub new {
    my $class = shift;
    local %_ = @_;
    my $self = {};
    $self->{fh} = $_{fh};
    $self->{file} = $_{file};
    $self->{http_code} = $_{temp} ? 302 : 301;
    if ($_{indent}) {
	$self->{indent} = ' ' x $_{indent};
    }
    $self->{nl} = 1;
    bless $self, $class
}

sub open {
    my $self = shift;
    return if $self->{fh};
    if (!$self->{file}) {
	$self->{fh} = \*STDOUT;
	return;
    }
    open($self->{fh}, '>', $self->{file})
	or croak "can't open $self->{file} for writing: $!";
    $self->{need_close} = 1;
}

sub close {
    my $self = shift;
    if ($self->{need_close}) {
	close $self->{fh};
	delete $self->{fh};
	delete $self->{need_close};
    }
}

sub indent {
    my ($self, $level) = @_;
    if ($level) {
	$self->{indent} = ' ' x $level
    } else {
	unset $self->{indent};
    }
}	

{
    no strict 'refs';
    foreach my $a (qw(fh file http_code need_close)) {
	*{ $a } = sub {
	    my $self = shift;
	    if (@_) {
		$self->{$a} = shift;
	    }
	    $self->{$a}
	};
	*{ 'unset_' . $a } = sub { delete shift->{$a} };
    }
}

sub print {
    my $self = shift;
    my $fh = $self->fh;
    my $s = join('',@_);
    if ($self->{indent}) {
	if ($self->{nl}) {
	    $s =~ s{^}{$self->{indent}}mg;
	} else {
	    $s =~ s{(?<!\A)^}{$self->{indent}}mg;
	}
	$self->{nl} = $s =~ /\n\z/;
    }
    CORE::print $fh $s;
}

sub printf {
    my ($self, $format, @args) = @_;
    $self->print(sprintf $format, @args);
}

sub getopt {
    my $self = shift;
    my %opts = @_;

    $opts{'shorthelp|?'} = sub {
        pod2usage(-message => $self->pod_usage_msg,
                  -input => pod_where({-inc => 1}, ref($self)),
                  -exitstatus => 0)
    };
    $opts{help} = sub {
        pod2usage(-exitstatus => 0,
                  -verbose => 2,
                  -input => pod_where({-inc => 1}, ref($self)))
    };
    $opts{usage} = sub {
        pod2usage(-exitstatus => 0,
                  -verbose => 0,
                  -input => pod_where({-inc => 1}, ref($self)))
    };

    my %optdef;
    foreach my $k (keys %opts) {
	if (ref($opts{$k}) eq 'CODE') {
	    $optdef{$k} = sub { &{$opts{$k}}($self, @_ ) }
	} elsif (ref($opts{$k})) {
	    $optdef{$k} = $opts{$k};
	} else {
	    $optdef{$k} = \$self->{options}{$opts{$k}};
	}
    }
    GetOptions(%optdef) or die "command line parsing error";
}

sub pod_usage_msg {
    my ($self, $input) = @_;
    my %args;

    my $msg = "";

    CORE::open my $fd, '>', \$msg;

    if (defined($input)) {
	$args{-input} = $input;
    } else {
        if (my $r = ref($self)) {
            $self = $r;
        }
        $args{-input} = pod_where({-inc => 1}, $self);
    }

    pod2usage(-verbose => 99,
              -sections => 'NAME',
              -output => $fd,
              -exitval => 'NOEXIT',
              %args);

    my @a = split /\n/, $msg;
    if ($#a < 1) {
        croak "missing or malformed NAME section in " . $args{-input} // $0;
    }
    $msg = $a[1];
    $msg =~ s/^\s+//;
    $msg =~ s/ - /: /;
    return $msg;
}

1;
