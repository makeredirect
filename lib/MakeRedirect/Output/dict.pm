package MakeRedirect::Output::dict;
use parent 'MakeRedirect::Output';
use strict;
use warnings;
use Carp;
use String::Escape qw( unprintable );
use MakeRedirect::Rule;

sub new {
    my $class = shift;
    local %_ = @_;
    my $d1 = delete $_{d1};
    my $d2 = delete $_{d2};
    my $self = $class->SUPER::new(%_);
    $self->{d1} = $d1 // "\n";
    $self->{d2} = $d2 // ' ';
    $self;
}

sub getopt {
    my $self = shift;
    $self->SUPER::getopt(
	'd1=s' => sub {
	    my ($self,undef,$val) = @_;
	    $self->{d1} = unprintable($val);
	},
	'd2=s' => sub {
	    my ($self,undef,$val) = @_;
	    $self->{d2} = unprintable($val);
	}
    );
}

sub ruleset {
    my ($self, $host, $rules) = @_;
    foreach my $key (sort { $b cmp $a } keys %{$rules}) {
	my $r = $rules->{$key};
	my $src = $r->src->clone;
	$src->unset_scheme;
	$self->print($src, $self->{d2}, $r->dst, $self->{d1});
	if ($r->www && (my $host = $src->host)) {
	    if ($host !~ s{^www\.}{}) {
		$host = 'www.' . $host
	    }
	    $src->host($host);
	    $self->print($src, $self->{d2}, $r->dst, $self->{d1});
	}
	if ($r->isflag(RF_EXACT|RF_NOSUB)) {
	    warn $r->locus . ": warning: flags not supported in dict format\n";
	}
    }
}

1;

=head1 NAME

dict - create a list of redirects in simple dictionary format

=head1 SYNOPSIS

B<makeredirect dict>
[B<--d1> I<DELIM1>]
[B<--d2> I<DELIM2>]
I<FILE> ...

=head1 DESCRIPTION

Converts a CSV file with source-destination URL pairs to a set of equivalent
I<dictionary> pairs.  Each pair consists of sourse and destination URLs with
the scheme stripped, separated by the I<DELIM1> string (a single space by
default) and terminated by the I<DELIM2> (a I<newline> character by default).

The resulting output can be used as input for B<vmod_dict>(3) module for
B<Varnish Cache>.

=head2 OPTIONS

=over 4

=item B<--d1> I<DELIM1>

Sets the value of the field delimiter.

=item B<--d2> I<DELIM2>

Sets the value of the record terminating delimiter.

=item B<-?>

Produce a short help text and exit.

=item B<--help>

Display the manual page and exit.

=item B<--usage>

Display a short command line usage summary and exit.

=back

=head1 SEE ALSO

B<makeredirect>(1),
L<MakeRedirect>,
B<vmod_dict>(3),
L<https://www.gnu.org.ua/software/vmod-dict>.

=cut
    
