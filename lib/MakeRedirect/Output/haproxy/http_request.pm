package MakeRedirect::Output::haproxy::http_request;
use parent 'MakeRedirect::Output';
use strict;
use warnings;
use Carp;
use MakeRedirect::Rule;
use File::Path qw(make_path);

sub getopt {
    my $self = shift;
    $self->SUPER::getopt(
	'T|threshold=n' => sub {
	    my ($self,undef,$val) = @_;
	    $self->{map_threshold} = $val;
	},
	'm|mapdir|map-directory=s' => sub {
	    my ($self,undef,$val) = @_;
	    $self->{mapdir} = $val;
	},
	'rm|clear' => sub {
	    my ($self) = @_;
	    $self->{cleardir} = 1;
	},
	't|temporary' => sub { shift->http_code(302) },
        'p|permanent' => sub { shift->http_code(301) },
    );
    $self->{map_threshold} //= 5;
    $self->{remove_queue} = {};
}

sub mapdir { shift->{mapdir} }

sub map_threshold { shift->{map_threshold} }

sub ruleset {
    my ($self, $host, $rules) = @_;

    foreach my $r (values %{$rules}) {
	my $last_match;
	my $last_match_len = 0;

	foreach my $g (@{$self->{hostrules}{$host}{$r->flags}}) {
	    my $len = $g->lcp($r);
	    if ($len > $last_match_len) {
		$last_match_len = $len;
		$last_match = $g;
	    }
	}
		
	unless ($last_match) {
	    $last_match = new MakeRedirect::PrefixGroup;
	    push @{$self->{hostrules}{$host}{$r->flags}}, $last_match;
	}
	$last_match->add($r);
    }
}

sub redirect_plain {
    my ($self, $host, $rules) = @_;
    foreach my $r (sort { $b->src->canonical cmp $a->src->canonical } @{$rules}) {
	my @stmt = qw(http-request redirect location);

	my $query;
	if ($r->noquery) {
	    $query = $r->dst->query;
	    $query = undef if ($query && $query eq '');
	}
	if ($r->nosub && !$query && $r->noquery) {
	    push @stmt, $r->dst;
	} else {
	    my $s = $r->dst->path;

	    if ($r->dst->host) {
		$s = $r->dst->scheme_prefix . $r->dst->host . $s;
	    }

	    unless ($r->nosub) {
  	        my $n = (() = $r->src->path =~ m{/}g)+1;
		unless ($r->src->path =~ m{/$}) {
		    $n++;
		}
		
		$s =~ s{/$}{} unless $n > 2;
		$s .= '%[' . (($r->noquery || $query) ? 'path' : 'capture.req.uri');
	        $s .= ",field($n,/,0)" if $n > 2;
	        $s .= ']';
	    }
	    if ($query) {
		$s .= '?' . $query
	    } elsif (!$r->noquery) {
		if ($r->nosub) {
		    # FIXME: This can be simplified to just ?%[query] if we can
		    # tolerate empty ? at the end of URI.
		    $s .= '%[query,length,bool,iif(?,)]%[query]';
		}
	    }
	    push @stmt, $s
	}
	push @stmt, 'code';
	push @stmt, ($r->temp ? 302 : $self->http_code);
	push @stmt, 'if';
	if ($host) {
	    push @stmt, '{', 'hdr(host)';
	    if ($r->www) {
   	        $host =~ s{^www\.}{};
		push @stmt, qw(-m reg -i), '(www\.)?' . quotemeta($host);
	    } else {
		push @stmt, '-i', $host;
	    }
	    push @stmt, '}';
	}
	unless ($r->src->path eq '/') {
	    if ($r->exact) {
		push @stmt, '{', 'path', $r->src->path, '}';
	    } else {
	        push @stmt, '{', 'path', '-m', 'beg', $r->src->path, '}',
	 	            '{', 'path', '-m', 'dir', $r->src->path, '}';
	    }
	}
	if ($r->src->query) {
	    push @stmt, '{', 'query', $r->src->query, '}';
	}
	$self->print(join(' ', @stmt)."\n");
    }
}

sub redirect_map {
    my ($self, $host, $rules) = @_;
    
    # Create map file
    my $filename = File::Spec->catfile($self->mapdir,
				       ($host eq '' ? '_DEFAULT' : $host) . ".map");
    open(my $fh, '>', $filename)
	or croak "can't open $filename for writing: $!";
    delete $self->{remove_queue}{$filename};
    foreach my $r (@{$rules}) {
	print $fh $r->src->path;
	if ($r->src->query) {
	    print $fh '?' . $r->src->query;
	}
	print $fh "\t";
	
        my $s = $r->dst->path;
	if ($r->dst->host) {
	    $s = $r->dst->scheme_prefix . $r->dst->host . $s;
	}
	if ($r->noquery) {
	    if ($r->dst->query) {
		$s .= '?' . $r->dst->query;
	    }
	}
	print $fh "$s\n";
    }
    close($fh);
    
    # Create HAProxy rule
    my $r = $rules->[0];
    my @stmt = qw(http-request redirect location);
    my $map = sprintf('%s,map(%s)',
		      $r->noquery ? 'path' : 'capture.req.uri',
		      $filename);
    my $s = "%[$map]";

    unless ($r->nosub) {
	my $n = (() = $r->src->path =~ m{/}g)+1;
	unless ($r->src->path =~ m{/$}) {
	    $n++;
	}
    	if ($n > 2) {
    	    $s .= '%[' . ($r->noquery ? 'path' : 'capture.req.uri');
    	    $s .= ",field($n,/,0)" if $n > 2;
    	    $s .= ']';
    	}
    }

    unless ($r->noquery) {
	if ($r->nosub) {
	    # FIXME: This can be simplified to just ?%[query] if we can
	    # tolerate empty ? at the end of URI.
	    $s .= '%[query,length,bool,iif(?,)]%[query]';
	}
    }
    push @stmt, $s;

    push @stmt, 'code';
    push @stmt, ($r->temp ? 302 : $self->http_code);
    push @stmt, 'if';
    if ($host) {
	push @stmt, '{', 'hdr(host)';
	if ($r->www) {
	    $host =~ s{^www\.}{};
	    push @stmt, qw(-m reg -i), '(www\.)?' . quotemeta($host);
	} else {
	    push @stmt, '-i', $host;
	}
	push @stmt, '}';
    }
    push @stmt, '{', $map, qw(-m found });	
    $self->print(join(' ', @stmt)."\n");
}

sub close {
    my $self = shift;

    if ($self->mapdir) {
	unless (-d $self->mapdir) {
	    make_path($self->mapdir, { error => \my $err });
	    if (@$err) {
		for my $diag (@$err) {
		    my ($file, $message) = %$diag;
		    if ($file eq '') {
			warn "$message\n";
		    } else {
			warn "$file: $message\n";
		    }
		}
		exit(1);
	    }
	} elsif ($self->{cleardir}) {
	    foreach my $f (glob File::Spec->catfile($self->mapdir, "*.map")) {
		$self->{remove_queue}{$f} = 1;
	    }
	}
    }

    foreach my $host (sort keys %{$self->{hostrules}}) {
	foreach my $g (sort { $b->max_len <=> $a->max_len }
		       map { @{$_} } values %{$self->{hostrules}{$host}}) {
	    if (($g->flags & RF_EXACT) && $self->mapdir && $g->count >= $self->map_threshold) {
		$self->redirect_map($host, $g->rules);
	    } else {
		$self->redirect_plain($host, $g->rules);
	    }
	}
    }

    foreach my $file (keys %{$self->{remove_queue}}) {
	unlink $file or warn "can't remove $file: $!\n";
    }
    
    $self->SUPER::close(@_)
}

package MakeRedirect::PrefixGroup;
use strict;
use warnings;

# MakeRedirect::PrefixGroup holds a list of rules (MakeRedirect::Rule) with
# the same flags and keeps track of the longest common prefix of source
# URIs in the group.

# Create new object:
sub new {
    my ($class) = @_;
    bless { max_len => 0, prefix_len => 0, rules => [] };
}

# Return the length of the longest source in the group.
sub max_len { shift->{max_len} }

# Return the longest common prefix length.
sub prefix_len { shift->{prefix_len} }

# Return the longest common prefix.
sub prefix {
    my $self = shift;
    return unless $self->prefix_len > 0;
    my @p = $self->splitdir($self->source(0));
    return join('/',@p[0..$self->prefix_len-1]);
}

# Return number of rules in the group.
sub count { 0+@{shift->{rules}} }

# Return a reference to the rule list.
sub rules { shift->{rules} }

# $x->source($n)
# Return the path of the $nth source.
sub source {
    my ($self, $n) = @_;
    return $self->{rules}[$n]->src->path;
}

# Return flags (common for all rules in the group).
sub flags {
    my ($self) = @_;
    return 0 unless @{$self->{rules}};
    return $self->{rules}[0]->flags;
}

# $x->add($r,...)
# Add one or more rules ($r) to the group. Recompute LCP length.
sub add {
    my $self = shift;
    for (@_) {
        push @{$self->{rules}}, $_;
	$self->{prefix_len} = $self->lcp($_);
	my $len = length($_->src->path);
	if ($len > $self->{max_len}) {
	    $self->{max_len} = $len;
	}
    }
}

# $x->lcp($rule)
# Compute the LCP length of the rules group and new $rule.
sub lcp {
    my ($self, $rule) = @_;
    my $dir = $rule->src->path;
    return 0 + $self->splitdir($dir) if $self->prefix_len == 0;
    my $len = $self->lcplen($self->source(0), $dir);
    return $len < $self->prefix_len ? $len : $self->prefix_len;
}

# ######################
# Auxiliary functions.
# ######################

# $x->splitdir($dir)
# Split the directory $dir into a list of directory components.
sub splitdir {
    my ($self, $dir) = @_;
    split m{/}, $dir;
}

# $x->lcplen($a,$b)
# Return the length (in directory components) of the longest common prefix
# between two strings.
sub lcplen {
    my ($self, $a, $b) = @_;
    my @ax = $self->splitdir($a);
    my @bx = $self->splitdir($b);
    my $len = @ax;
    $len = @bx if @bx < $len;
    my $i;
    for ($i = 0; $i < $len; $i++) {
	last unless $ax[$i] eq $bx[$i];
    }
    return $i;
}

1;

=head1 NAME

http-request - generate http-request redirect statements for HAProxy

=head1 SYNOPSIS

B<makeredirect> [B<OPTIONS>] haproxy http-request I<FILE>...

=head1 DESCRIPTION

Converts a CSV file with source-destination URL pairs to a set of equivalent
B<http-request redirect> statements, suitable for inclusion into
B<haproxy.cfg> file.

=head1 SEE ALSO

B<makeredirect>(1),
L<http://www.gnu.org.ua/software/haproxy-bulkredirect>,
L<MakeRedirect>.

=cut
