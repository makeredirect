package MakeRedirect::Output::haproxy::help;
use parent 'MakeRedirect::Output::help';
use strict;
use warnings;

sub getopt {
    my $self = shift;
    $self->generic_getopt(args => \@_,
			  header => 'Available haproxy submodules are:',
			  help => 'Run "makeredirect haproxy help NAME" to obtain help for a particular module.');
}

1;
