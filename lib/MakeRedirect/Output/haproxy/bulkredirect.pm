package MakeRedirect::Output::haproxy::bulkredirect;
use parent 'MakeRedirect::Output';
use strict;
use warnings;
use Carp;
use MakeRedirect::Rule;

sub flags_to_options {
    my ($self, $flags) = @_;
    
    my @optname = (
	RF_EXACT, 'exact',
	RF_NOSUB, 'strippath',
	RF_TEMP, 'temporary',
	RF_WWW, 'www',
	RF_NOQUERY, 'stripquery'
    );

    my @opt;
    for (my $i = 0; $i < @optname; $i += 2) {
	if  ($optname[$i] & $flags) {
	    push @opt, $optname[$i + 1];
	}
    }

    return join(',', @opt);
}

sub ruleset {
    my ($self, $host, $rules) = @_;

    # Compute common flags
    my $comflag = 0;
    foreach my $r (values %{$rules}) {
	$comflag |= ~($r->flags & RF_ALL);
    }
    $comflag = ~$comflag & RF_ALL;

    $self->printf("[%s]\n", $host eq '' ? '*' : $host);
    if ($comflag) {
	$self->printf("options %s\n", $self->flags_to_options($comflag));
    }
    foreach my $r (values %{$rules}) {
	$self->print($r->src->path);
	if ($r->src->query) {
	    $self->print('?' . $r->src->query);
	}
	$self->print("\t");
	print $r->dst;
	if (my $xf = $r->flags & ~($r->flags & $comflag)) {
	    $self->printf("\t%s", $self->flags_to_options($xf));
	}
	$self->print("\n");
    }
}

1;

=head1 NAME

bulkredirect - generate input file for the bulkredirect.lua module

=head1 SYNOPSIS

B<makeredirect>
B<haproxy>
B<bulkredirect>
I<FILE> ...

=head1 DESCRIPTION

Converts a CSV file with source-destination URL pairs to an equivalent
redirection table file for the B<bulkredirect.lua> module.

=head1 OPTIONS

=over 4

=item B<-?>

Produce a short help text and exit.

=item B<--help>

Display the manual page and exit.

=item B<--usage>

Display a short command line usage summary and exit.

=back

=head1 SEE ALSO

B<makeredirect>(1),
B<makeredirect-haproxy>(1),
L<http://puszcza.gnu.org.ua/software/haproxy-bulkredirect>.

=cut

