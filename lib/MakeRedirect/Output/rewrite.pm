package MakeRedirect::Output::rewrite;
use strict;
use warnings;
use Carp;
use File::Spec;
use parent 'MakeRedirect::Output';
use MakeRedirect::URI::SRC;
use MakeRedirect::Rule;

sub new {
    my $class = shift;
    local %_ = @_;
    my $monolithic = delete $_{monolithic};
    my $self = $class->SUPER::new(%_);
    $self->{monolithic} = $monolithic // 1;
    $self
}

sub getopt {
    my $self = shift;
    $self->SUPER::getopt(
	'm|monolithic' => sub {
	    my ($self,undef,$val) = @_;
	    $self->{monolithic} = 1;
	},
	's|split' => sub {
	    my ($self,undef,$val) = @_;
	    $self->{monolithic} = 0;
         },	
	'o|output=s' => sub {
	    my ($self,undef,$val) = @_;
	    $self->{file} = $val;
	},
	't|temporary' => sub { shift->http_code(302) },
        'p|permanent' => sub { shift->http_code(301) },
        'indent|i=s' => sub { shift->indent($_[1]) }
    );
}

sub open {
    my $self = shift;
    if ($self->{monolithic}) {
	return $self->SUPER::open(@_);
    }
}

sub ruleset {
    my ($self, $host, $rules) = @_;
    my $fh;
    if (!$self->fh) {
	my $filename = $self->{monolithic}
	    ? $self->{file}
	    : File::Spec->catfile($self->{file}, ($host || 'GENERIC'))
	    . '.conf';
        CORE::open($fh, '>', $filename)
	    or croak "can't open output file $filename: $!";
	$self->fh($fh);
	$self->need_close($fh);
    }
    
    if (!$self->{monolithic} && $host ne '') {
	$self->print("# Host $host\n");
    }
    my @prereq;
    if ($self->{monolithic} && $host ne '') {
	$prereq[0] = sprintf("RewriteCond %%{HTTP_HOST} =%s [NC]\n", $host);
	$host =~ s{^www\.}{};
	$host =~ s{\.}{\\.}g;
	$prereq[1] = sprintf("RewriteCond %%{HTTP_HOST} ^(www\\.)?%s\$ [NC]\n",
			     $host);
    }
    foreach my $key (sort { $b cmp $a } keys %{$rules}) {
	my $r = $rules->{$key};
	my $src = new MakeRedirect::URI::SRC($r->src);
	my $dst = $r->dst;
	if (@prereq) {
	    $self->print($prereq[$r->www])
	}
	if ($src->query) {
	    $self->printf("RewriteCond %%{QUERY_STRING} %s\n", $src->query);
	    $src->unset_query
	}
	my $flags = 'L,R='.($r->temp ? 302 : $self->http_code);
	if ($dst =~ m{%[0-9a-fA-F]{2}}) {
	    $flags .= ',NE';
	}
	if ($r->noquery && !$r->dst->query) {
	    $flags .= ',QSD';
	}
	my $src_path = $src->path || '/';
        my $dst_path = $dst->path || '/';	
	if ($r->exact) {
	    $dst->path($dst_path);
	    $self->printf("RewriteRule \"^%s\$\" %s [%s]\n",
			  $src_path, $dst, $flags);
	} elsif ($r->nosub) {
  	    $self->printf("RewriteRule \"^%s\" %s [%s]\n",
			  $src_path, $dst, $flags);	    
	} else {
	    if ($src_path eq '/') {
		$src_path = '(.*)';
	    } else {
	        $src_path =~ s{/?$}{(/.*)?};
	    }
            $dst_path =~ s{/?$}{\$1};
	    $dst->path($dst_path);
  	    $self->printf("RewriteRule \"^%s\" %s [%s]\n",
			  $src_path, $dst, $flags);
	}
    }
    if ($fh) {
	$self->close
    }
}

1;

=head1 NAME

rewrite - generate redirection rules for mod_rewrite

=head1 SYNOPSIS

B<makeredirect>
B<rewrite>
[B<-i> I<N>]
[B<-mpst>]
[B<--indent=>I<N>]
[B<--monolithic>]
[B<--split>]
[B<--permanent>]
[B<--temporary>]
I<FILE> ...

=head1 DESCRIPTION

Converts a CSV file with source-destination URL pairs to a set of equivalent
B<mod_rewrite> rules.  The output can be a I<monolithic> single set of rewrite
rules or multiple sets of rules scattered among several output files named by
the B<ServerName> they refer to.

In the I<monolithic> mode, each line of the source CSV file that contains
a domain name in the column 1 produces a B<RewriteRule> directive preceded by
a B<RewriteCond> that limits its scope to the given domain name only.  For
example, the input line

     http://example.com/login,/

will produce two lines:

     RewriteCond %{HTTP_HOST} =example.com [NC]
     RewriteRule "^/login" / [L,R=301]

whereas the line

     /logout,/exit

produces

     RewriteRule "^/logout" /exit [L,R=301]

The monolithic mode is the default.

In I<split> mode, all rules for a specific domain are grouped together and
saved in a file named by the domain name with the C<.conf> suffix.  The
argument to the B<--output> (B<-o>) option supplies the name of the directory
where these files will be stored.  The output directory must exist.

=head1 OPTIONS

=over 4

=item B<-i>, B<--indent=>I<N>

Indent each output line I<N> characters.

=item B<-m>, B<--monolithic>

Enable monolithic mode (default).

=item B<-p>, B<--permanent>

Create permanent redirects (default).

=item B<-s>, B<--split>

Enable split mode.

=item B<-t>, B<--temporary>

Create temporary redirects.

=item B<-?>

Produce a short help text and exit.

=item B<--help>

Display the manual page and exit.

=item B<--usage>

Display a short command line usage summary and exit.

=back

=head1 SEE ALSO

B<makeredirect>(1),
L<MakeRedirect>.

=cut

