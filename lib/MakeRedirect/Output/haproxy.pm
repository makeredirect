package MakeRedirect::Output::haproxy;
use strict;
use warnings;
use Carp;

sub new {
    my $class = shift;
    my @classpath = split(/::/, $class);
    bless { classpath => \@classpath, args => \@_ }, $class
}

sub classpath { @{shift->{classpath}} }
sub output { shift->{output} }

sub getopt {
    my $self = shift;

    my $submodule = shift @ARGV
	or die "submodule name must be specified\n";

    $submodule =~ s/-/_/g;
    my $modname = __PACKAGE__ . '::' . $submodule;
    (my $modpath = $modname) =~ s{::}{/}g;
    $modpath .= '.pm';
    
    $self->{output} = eval { require $modpath; $modname->new(@{$self->{args}}) };
    if ($@) {
        if ($@ =~ /Can't locate $modpath/) {
	    die "unknown output module: haproxy $submodule\n"
        }
        croak $@;
    }
    $self->{output}->getopt(@_);
}

our $AUTOLOAD;

sub AUTOLOAD {
    my $self = shift;
    (my $meth = $AUTOLOAD) =~ s/.*:://;
    $self->output->${\$meth}(@_)
}

sub DESTROY {}

1;
=head1 NAME

haproxy - generate redirect statements for HAProxy

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 OPTIONS

=head1 SEE ALSO

B<makeredirect>(1),
L<http://www.gnu.org.ua/software/haproxy-bulkredirect>,
L<MakeRedirect>.

=cut
