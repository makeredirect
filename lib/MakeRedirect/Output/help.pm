package MakeRedirect::Output::help;

=head1 NAME

help - prints available modules with short descriptions

=head1 SYNOPSIS

B<makeredirect help> [I<NAME>]

=head1 DESCRIPTION

Without arguments, prints a list of available B<makeredirect> modules
with short descriptions.

Given a single argument I<NAME>, looks up a module of that name and,
if found, displays its manual page.

=head1 OPTIONS

=over 4

=item B<-?>

Produce a short help text and exit.

=item B<--help>

Display the manual page and exit.

=item B<--usage>

Display a short command line usage summary and exit.

=back

=head1 SEE ALSO

B<makeredirect>(1),
L<MakeRedirect>.

=cut

use strict;
use warnings;
use Carp;
use File::Basename;
use File::Spec;
use Pod::Usage;
use Pod::Find qw(pod_where);
use parent 'MakeRedirect::Output';

sub new {
    my $class = shift;
    my @classpath = split(/::/, $class);
    pop @classpath;
    bless { classpath => \@classpath }, $class
}

sub classpath { @{shift->{classpath}} }

sub module_list {
    my $self = shift;
    @{$self->{module_list} //=
	[sort { $a->[0] cmp $b->[0] }
	 map {
	     my $name = basename($_);
	     my $filename = File::Spec->catfile($self->classpath, $name);
	     if (exists($INC{$filename})) {
		 ()
	     } else {
		 local $SIG{__WARN__} = sub {};
		 eval {
		     require $filename;
                 };
	         $name =~ s/\.pm$//;
                 $@ ? () : [$name, $_];
	     }
         }
         map { glob File::Spec->catfile($_, $self->classpath, '*.pm') } @INC]}
}

sub generic_getopt {
    my $self = shift;
    local %_ = @_;

    $self->SUPER::getopt(@{$_{args}});
    if (@ARGV) {
	my $name = shift @ARGV;
	if (@ARGV) {
	    pod2usage(-exitstatus => 1,
                      -verbose => 0,
		      -output => \*STDERR);
	}
	my ($module) = grep { $_->[0] eq $name } $self->module_list;
	if ($module) {
	    pod2usage(-verbose => 2,
		      -input => $module->[1],
                      -exitstatus => 0)
	} else {
	    print "Unknown module $name\n";
	}
    }

    print $_{header} || "Available modules are:";
    print "\n\n";

    foreach my $mod ($self->module_list) {
	my $s;
        CORE::open(my $fh, '>', \$s);
        pod2usage(-input => $mod->[1],
                  -output => $fh,
                  -verbose => 99,
	          -sections => ['NAME'],
	          -exitstatus => 'NOEXIT');
        CORE::close $fh;
	my (undef,$descr) = split("\n", $s||'');
        unless ($descr) {
            $descr = '    ' . $mod->[0]
        }
	print "$descr\n";
    }
    print "\n";
    print $_{help} || "Run \"makeredirect help NAME\" to obtain help for a particular module.";
    print "\n";
    exit(0)
}

sub getopt {
    my $self = shift;
    $self->generic_getopt(args => \@_);			  
}

1;

