package MakeRedirect::Rule;
use strict;
use warnings;
require Exporter;

our @ISA = qw(Exporter);
our @EXPORT = qw(RF_WWW RF_EXACT RF_NOSUB RF_PATH_MASK RF_TEMP RF_NOQUERY RF_ALL);

use constant {
    RF_EXACT => 0x01,   # Exact match
    RF_NOSUB => 0x02,   # Drop subdirectories from source
    RF_PATH_MASK => 0x03,
    RF_TEMP  => 0x04,   # Create temporary redirect
    RF_WWW   => 0x08,   # Create rules for both DOMAIN and www.DOMAIN
    RF_NOQUERY => 0x10, # Drop query
};

use constant RF_ALL => RF_EXACT | RF_NOSUB | RF_TEMP | RF_WWW | RF_NOQUERY;

sub new {
    my ($class, $src, $dst, $flags, $locus) = @_;
    bless { src => $src, dst => $dst, flags => $flags, locus => $locus },
	  $class
}

sub src { shift->{src} }
sub dst { shift->{dst} }
sub flags { shift->{flags} }
sub isflag {
    my ($self, $f) = @_;
    return ($self->{flags} & $f) != 0
}
sub www   { shift->isflag(RF_WWW) }
sub exact { shift->isflag(RF_EXACT) }
sub nosub { shift->isflag(RF_NOSUB|RF_EXACT) }
sub noquery { shift->isflag(RF_NOQUERY) }
sub temp  { shift->isflag(RF_TEMP) }
sub locus { shift->{locus} }

1;
