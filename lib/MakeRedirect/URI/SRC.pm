package MakeRedirect::URI::SRC;
use strict;
use warnings;
use parent 'MakeRedirect::URI';
use MakeRedirect::URI;
use URI::Encode qw(uri_decode);

sub new {
    my ($class, $orig) = @_;
    if (ref($orig) eq '') {
	$orig = new MakeRedirect::URI($orig);
    }
    bless $orig->clone, $class;
}

sub path {
    my $self = shift;
    uri_decode($self->SUPER::path(@_));
}

1;
