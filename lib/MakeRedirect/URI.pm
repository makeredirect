package MakeRedirect::URI;
use strict;
use warnings;
use Clone;
use URI::Encode qw(uri_encode);
use Net::IDN::Encode qw(domain_to_ascii);
    
sub new {
    my ($class, $url, %args) = @_;
    my $self;
    if ($url =~ m{^(?: (?<scheme> https? ):// )?
	           (?<host> (?: [\w-]+\. )+[\w]+ )
	           (?<path> /.*? )?
	           (?: \? (?<query> .* ))? $}x) {
        $self = { scheme => $+{scheme},
	          host => $+{host},
	          path => $+{path} // '/',
	          query => $+{query} };
	$self->{host} = domain_to_ascii($self->{host});
    } elsif ($url =~ m{^/}) {
        $self = { path => $url }
    } else {
        $self = { path => '/' . $url }
    }

    foreach my $k (qw(scheme host path)) {
	unless ($self->{$k}) {
	    $self->{$k} = delete $args{$k};
	}
    }
    if ($self->{host} && !$self->{scheme}) {
	$self->{scheme} = 'http';
    }
    if ($self->{path}) {
	if ($self->{path} !~ m{%[0-9a-fA-F]{2}}) {
	    $self->{path} = uri_encode($self->{path});
	}
    }
    bless $self, $class;
}

sub clone {
    my $self = shift;
    return Clone::clone($self);
}

my @attributes = qw(scheme host path query);

{
    no strict 'refs';
    foreach my $a (@attributes) {
	*{ $a } = sub {
	    my $self = shift;
	    if (@_) {
		$self->{$a} = shift;
	    }
	    $self->{$a}
	};
	*{ 'unset_' . $a } = sub { delete shift->{$a} };
    }
}

sub scheme_prefix {
    my ($self) = @_;
    $self->{scheme} ? ($self->scheme . '://') : '';
}

sub local {
    my ($self) = @_;
    my $s = $self->{path} || '/';
    if ($self->query) {
	$s .= '?' . $self->query;
    }
    return $s;
}    

sub canonical {
    my ($self) = @_;
    my $s = $self->local;
    if ($self->host) {
	$s = $self->scheme_prefix . $self->host . $s;
    }
    return $s;
}

sub query_normalized {
    my ($self) = @_;
    join('&', sort split(/&/, $self->query));
}

use overload
    '""' => sub {
	my ($self) = @_;
	return $self->canonical;
    },
    '==' => sub {
	my ($self, $other) = @_;
        (($self->{scheme} // '') eq ($other->{scheme} // '')) &&
   	    (($self->{host} // '') eq ($other->{host} // '')) &&
	    (($self->{path} // '') eq ($other->{path} // '')) &&
	    (($self->{query} // '') eq ($other->{query} // '')) 
    },
    '!=' => sub {
	my ($self, $other) = @_;
	!($self == $other);
    };

1;

