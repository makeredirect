# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test::More tests => 5;

use_ok 'MakeRedirect::URI::SRC';

my $uri = new MakeRedirect::URI::SRC('https://example.com/foo%20bar?q=http%3A%2F%2Fexample.org');
is($uri->scheme,'https');
is($uri->host,'example.com');
is($uri->path,'/foo bar');
is($uri->query,'q=http%3A%2F%2Fexample.org');

