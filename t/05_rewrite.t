use lib qw(t lib);
use strict;
use warnings;
use File::Temp;
use Test::More tests => 3;

use_ok 'MakeRedirect';

my $fh = File::Temp->new();

my $mr = new MakeRedirect('rewrite', fh => $fh);
ok($mr->read(\*DATA, filename => $0), 'read input');
$mr->output;

my $expect = q{RewriteRule "^/foo(/.*)?" /bar$1 [L,R=301]
RewriteCond %{HTTP_HOST} =example.com [NC]
RewriteRule "^/app(/.*)?" http://example.com$1 [L,R=301]
RewriteCond %{HTTP_HOST} =example.com [NC]
RewriteRule "^(.*)" http://example.com/static$1 [L,R=301]
RewriteCond %{HTTP_HOST} =example.org [NC]
RewriteRule "^/foo/bar(/.*)?" http://example.com/BAR$1 [L,R=301]
RewriteCond %{HTTP_HOST} =example.org [NC]
RewriteRule "^/foo(/.*)?" http://example.com/other$1 [L,R=301]
};

local $/ = undef;
$fh->seek(0, 0);
my $text = <$fh>;
is($text, $expect, "monolithic output");

__DATA__
/foo,/bar
http://example.org/foo,http://example.com/other
http://example.org/foo/bar,http://example.com/BAR
http://example.com/app,http://example.com
http://example.com,http://example.com/static
