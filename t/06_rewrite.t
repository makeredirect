use lib qw(t lib);
use strict;
use warnings;
use File::Temp;
use File::Basename;
use Test::More tests => 12;

use_ok 'MakeRedirect';

my $dir = File::Temp->newdir();
my $mr = new MakeRedirect('rewrite', file => $dir, monolithic => 0);
ok($mr->read(\*DATA, filename => $0), 'read input');
$mr->output;

my %files = (
    'GENERIC.conf' => q{RewriteRule "^/foo(/.*)?" /bar$1 [L,R=301]
},
    'example.com.conf' => q{# Host example.com
RewriteRule "^/app(/.*)?" http://example.com$1 [L,R=301]
RewriteRule "^(.*)" http://example.com/static$1 [L,R=301]
},
    'example.org.conf' => q{# Host example.org
RewriteRule "^/foo/bar(/.*)?" http://example.com/BAR$1 [L,R=301]
RewriteRule "^/foo(/.*)?" http://example.com/other$1 [L,R=301]
});

is_deeply([sort grep { exists($files{$_}) } map { basename($_) } glob "$dir/*"],
 	  [sort keys %files],
	  "directory listing");

foreach my $file (sort keys %files) {
    my $name = File::Spec->catfile($dir, $file);
    ok(-f $name, "$file exists") or next;
    ok(open(FH, '<', $name),"$file open") or next;
    local $/ = undef;
    is(<FH>, $files{$file}, "$file content");
    close FH;
}
__DATA__
/foo,/bar
http://example.org/foo,http://example.com/other
http://example.org/foo/bar,http://example.com/BAR
http://example.com/app,http://example.com
http://example.com,http://example.com/static
