# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test::More tests => 3;

use MakeRedirect::Output;
my $s;
ok(open(FH, '>', \$s), 'Open output');
ok(my $out = new MakeRedirect::Output(fh => \*FH, indent=>4),
   'Create output');
$out->print('abc');
$out->print("d\nefg\n");
$out->print("i\njkl");
$out->print("mn\n");
close FH;
is($s, q{    abcd
    efg
    i
    jklmn
}, 'Indentation');

    
