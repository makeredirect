package TestRedirect;
use lib qw(t lib);
use strict;
use warnings;
use parent 'MakeRedirect';
use Carp;
use Test::More;

sub new {
    my $class = shift;
    my $module = shift;
    local %_ = @_;
    my $input = delete $_{input};
    my $s;
    open(my $fh, '>', \$s) or croak "can't open output string";
    my $self = $class->SUPER::new($module, %_, fh => $fh);
    $self->{s} = \$s;
    $self->{error_list} = [];
    if ($input) {
	open(my $in, '<', \$input);
        $self->read($in, filename => $0);
    } else {
        $self->read(\*main::DATA, filename => $0);
    }
    $self->output;
    $self;
}

sub test {
    my $class = shift;
    my $module = shift;
    local %_ = @_;
    my $expect_out = delete $_{output} || '';
    if ($expect_out !~ m{\n$}) {
	$expect_out .= "\n";
    }
    my $expect_err = delete $_{errors} || [];
    my $message = delete $_{message};
    if (!$message && (split(/\n/, $_{input})) == 1) {
	chomp($message = $_{input});
    }
    my $self = $class->new($module, %_);
    is_deeply([$self->errors], $expect_err,
	$message ? "$message: errors" : undef);
    is($self->result, $expect_out,
	$message ? "$message: output" : undef);
#    $numtests += 2;
    $self
}

sub error {
    my $self = shift;
    push @{$self->{error_list}}, "@_";
}

sub errors {
    my $self = shift;
    return @{$self->{error_list}};
}

sub success {
    my $self = shift;
    $self->errors == 0
}

sub result {
    my $self = shift;
    return ${$self->{s}};
}

1;
