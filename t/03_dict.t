# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test::More tests => 3;

use_ok 'TestRedirect';
my $t = new TestRedirect('dict');
is_deeply([$t->errors],[]);
is $t->result,<<EOT;
/foo /bar
example.com/source/subdir /subdir
example.com/source /dest
example.org/foobar /baz
EOT
;
__DATA__
/foo,/bar
https://example.com/source,/dest
http://example.org/foobar,/baz
https://example.com/source/subdir,/subdir
