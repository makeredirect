# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test::More tests => 27;

use_ok 'TestRedirect';

TestRedirect->test('rewrite',
		   monolithic => 1,
		   input => q{/foo,/bar},
		   output => q{RewriteRule "^/foo(/.*)?" /bar$1 [L,R=301]},
		   message => 'default rewrite');

TestRedirect->test('rewrite',
		   monolithic => 1,
		   input => q{/baz,/qux,exact},
		   output => q{RewriteRule "^/baz$" /qux [L,R=301]},
		   message => 'exact path rewrite');

TestRedirect->test('rewrite',
		   monolithic => 1,
		   input => q{/baz,/qux,nosub},
		   output => q{RewriteRule "^/baz" /qux [L,R=301]},
		   message => 'nosub path rewrite');

TestRedirect->test('rewrite',
		   monolithic => 1,
		   input => q{/baz,/qux,temp},
		   output => q{RewriteRule "^/baz(/.*)?" /qux$1 [L,R=302]},
		   message => 'temporary redirect');

TestRedirect->test('rewrite',
		   monolithic => 1,
		   input => q{/source/f%C3%B8rste%20lenke,/first},
		   output => q{RewriteRule "^/source/første lenke(/.*)?" /first$1 [L,R=301]},
		   message => q{special characters in source});

TestRedirect->test('rewrite',
		   monolithic => 1,
		   input => q{/baz,/sub/b a z},
		   output => q{RewriteRule "^/baz(/.*)?" /sub/b%20a%20z$1 [L,R=301,NE]},
		   message => q{special characters in destination});

TestRedirect->test('rewrite',
		   monolithic => 1,
		   input => q{/wer,/abr,nosub},
		   output => q{RewriteRule "^/wer" /abr [L,R=301]},
		   message => 'no subdir path rewrite');

TestRedirect->test('rewrite',
		   monolithic => 1,
		   input => q{example.org/foo,/bar},
		   output => q{RewriteCond %{HTTP_HOST} =example.org [NC]
RewriteRule "^/foo(/.*)?" /bar$1 [L,R=301]},
		   message => 'domain specific rewrite');

TestRedirect->test('rewrite',
		   monolithic => 1,
		   input => q{example.org,new.example.com},
		   output => q{RewriteCond %{HTTP_HOST} =example.org [NC]
RewriteRule "^(.*)" http://new.example.com$1 [L,R=301]},
		   message => 'domain-to-domain redirect');

TestRedirect->test('rewrite',
		   monolithic => 1,
		   input => q{/,/app
/static,/local
/static/user,/login
},
		   output => q{RewriteRule "^/static/user(/.*)?" /login$1 [L,R=301]
RewriteRule "^/static(/.*)?" /local$1 [L,R=301]
RewriteRule "^(.*)" /app$1 [L,R=301]
},
		   message => q{rule ordering});

TestRedirect->test('rewrite',
		   monolithic => 1,
		   input => q{example.org/foo,https://example.com,www
www.example.net/bar,https://example.com/baz,www},
		   output => q{RewriteCond %{HTTP_HOST} ^(www\.)?example\.org$ [NC]
RewriteRule "^/foo(/.*)?" https://example.com$1 [L,R=301]
RewriteCond %{HTTP_HOST} ^(www\.)?example\.net$ [NC]
RewriteRule "^/bar(/.*)?" https://example.com/baz$1 [L,R=301]},
		   message => q{www flag});

TestRedirect->test('rewrite',
		   monolithic => 1,
		   input => q{example.org/foo,https://example.com,www exact},
		   output => q{RewriteCond %{HTTP_HOST} ^(www\.)?example\.org$ [NC]
RewriteRule "^/foo$" https://example.com/ [L,R=301]},
		   message => q{www and exact});

TestRedirect->test('rewrite',
		   monolithic => 1,
		   input => q{-,,exact
/local,/static
/user,/login
/foo,/bar,-exact
-,,nosub
/upload,/in
/download,/out,-nosub
},
		   output => q{RewriteRule "^/user$" /login [L,R=301]
RewriteRule "^/upload" /in [L,R=301]
RewriteRule "^/local$" /static [L,R=301]
RewriteRule "^/foo(/.*)?" /bar$1 [L,R=301]
RewriteRule "^/download(/.*)?" /out$1 [L,R=301]
},
		   message => q{default flags});
