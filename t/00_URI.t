# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use Test::More tests => 18;

use_ok 'MakeRedirect::URI';

my $uri = new MakeRedirect::URI('https://example.com/foo/bar?q=1&a=f');
is($uri->scheme,'https');
is($uri->host,'example.com');
is($uri->path,'/foo/bar');
is($uri->query,'q=1&a=f');
is($uri->canonical,'https://example.com/foo/bar?q=1&a=f');

$uri = new MakeRedirect::URI('example.com/foo/bar?q=1');
is($uri->scheme,'http');
is($uri->host,'example.com');
is($uri->path,'/foo/bar');
is($uri->query,'q=1');
is($uri->canonical,'http://example.com/foo/bar?q=1');

$uri = new MakeRedirect::URI('foo/bar');
is($uri->scheme,undef);
is($uri->host,undef);
is($uri->path,'/foo/bar');
is($uri->query,undef);
is($uri->canonical,'/foo/bar');

my $a = new MakeRedirect::URI('http://example.com');
my $b = new MakeRedirect::URI('example.com');
ok($a == $b, 'URI equality');
$b = new MakeRedirect::URI('https://example.com');
ok($a != $b, 'URI inequality');

